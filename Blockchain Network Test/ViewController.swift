//
//  ViewController.swift
//  Test
//
//  Created by Nay Min Ko on 28/02/2021.
//

import UIKit
import BlockchainNetwork

typealias BlockHeight = UInt64

class ViewController: UIViewController {
    
    var height: BlockHeight = 0
    @IBOutlet weak var peerCountLabel: UILabel!
    @IBOutlet weak var heightTextField: UITextField!
    
    var network: BlockchainNetwork!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        network = BlockchainNetwork(delegate: self, peerNodeDelegate: self)
        network.startBrowsing()
    }

    @IBAction func requestHeight(_ sender: Any) {
        var zeroByte = UInt8()
        for peerNode in network.servers {
            peerNode.send(blockchainMessage: BlockchainMessage(messageType: .requestHeight, data: Data(bytes: &zeroByte, count: MemoryLayout<UInt8>.size)))
        }
    }
    
    @IBAction func toggleListen(_ sender: UISwitch) {
        if sender.isOn {
            network.startListening()
        } else {
            network.stopListening()
            for clientNode in network.clients {
                clientNode.stopConnection()
            }
        }
    }
    
    @IBAction func sendHeight(_ sender: Any) {
        for peerNode in network.servers {
            if let text = heightTextField.text, var height = BlockHeight(text) {
                peerNode.send(blockchainMessage: BlockchainMessage(messageType: .sendHeight, data: Data(bytes: &height, count: MemoryLayout<BlockHeight>.size)))
            } else {
                print("Needs a non-negative integer")
            }
        }
    }
}

extension ViewController: BlockchainNetworkDelegate, PeerNodeDelegate {
    func blockchainNetworkBrowseResultsDidUpdate() {
        network.connectToPeerNodes()
    }
    
    func blockchainPeerNodesDidUpdate() {
        peerCountLabel.text = "\(network.servers.count):\(network.clients.count)"
    }
    
    func peerNode(_ peerNode: PeerNode, didReceiveMessage message: BlockchainMessage) {
        switch message.messageType {
        case .requestHeight:
            if let text = heightTextField.text, var height = BlockHeight(text) {
                peerNode.send(blockchainMessage: BlockchainMessage(messageType: .sendHeight, data: Data(bytes: &height, count: MemoryLayout<BlockHeight>.size)))
            } else {
                print("Needs a non-negative integer")
            }
        case .sendHeight:
            let buffer = UnsafeMutableRawBufferPointer.allocate(byteCount: MemoryLayout<BlockHeight>.size, alignment: MemoryLayout<BlockHeight>.alignment)
            message.data.copyBytes(to: buffer, count: MemoryLayout<BlockHeight>.size)
            let height = buffer.load(as: BlockHeight.self)
            heightTextField.text = height.description
        default:
            break
        }
    }
}
